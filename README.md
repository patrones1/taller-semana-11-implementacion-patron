**Taller semana 11 - Implementación patrón**

- Dado un problema identificar qué patrón de diseño se podría utilizar.

- Hay variedad de pokemons y también existe una pokédex que te dice descripciones de pokemons.

- Cada pokédex pertenece a una persona, puede tener tu nombre y tu sello personal, pero se conecta a la base de datos única de pokemons para recuperar la información de cada uno.

- La idea es que dado el nombre de un pokemon podamos buscar en la pokédex y tengamos dicha información.

- Completar el ejercicio de pokemons y aplicar patrones singleton, builder y prototype.
