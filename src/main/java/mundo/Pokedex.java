package mundo;

import java.util.ArrayList;
import java.util.List;

public class Pokedex {
    private static Pokedex instancia = null;
    private List<Pokemon> listaPokemon;

    private Pokedex() {
        listaPokemon = new ArrayList<>();
    }

    public static Pokedex obtenerInstancia() {
        if (instancia == null) {
            instancia = new Pokedex();
        }
        return instancia;
    }

    public void agregarPokemon(Pokemon pokemon) {
        listaPokemon.add(pokemon);
    }

    public void eliminarPokemon(Pokemon pokemon) {
        listaPokemon.remove(pokemon);
    }

    public List<Pokemon> obtenerListaPokemon() {
        return listaPokemon;
    }
}