package mundo;

public class Pokemon implements Cloneable {
    private String nombre;
    private int nivel;
    private String tipo;

    public Pokemon(String nombre) {
        this.nombre = nombre;
        this.nivel = 1;
        this.tipo = "Normal";
    }

    // Constructor privado para utilizar en el método "clonar"
    private Pokemon(String nombre, int nivel, String tipo) {
        this.nombre = nombre;
        this.nivel = nivel;
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public Pokemon clone() {
        try {
            // Utilizar el constructor privado para crear una copia del objeto
            Pokemon copia = new Pokemon(this.nombre, this.nivel, this.tipo);
            return copia;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
