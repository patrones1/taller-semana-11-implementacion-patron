package mundo;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        // Agregar un pokemon a la pokedex
        Pokedex pokedex = Pokedex.obtenerInstancia();
        Pokemon pikachu = new Pokemon("Pikachu");
        pokedex.agregarPokemon(pikachu);

        // Obtener la lista de pokemon de la pokedex
        List<Pokemon> listaPokemon = pokedex.obtenerListaPokemon();

        // Eliminar un pokemon de la pokedex
        pokedex.eliminarPokemon(pikachu);


        // Crear un pokemon original
        Pokemon original = new Pokemon("Pikachu");
        original.setNivel(10);
        original.setTipo("Eléctrico");

        // Clonar el pokemon original
        Pokemon copia = original.clone();

        // Modificar el nombre y nivel del pokemon copiado
        copia.setNombre("Raichu");
        copia.setNivel(20);

        // Imprimir los atributos de los dos objetos
        System.out.println(original.getNombre() + " Nivel: " + original.getNivel() + " Tipo: " + original.getTipo());
        System.out.println(copia.getNombre() + " Nivel: " + copia.getNivel() + " Tipo: " + copia.getTipo());

    }
}